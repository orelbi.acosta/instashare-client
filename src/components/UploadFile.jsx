import { React, useState } from "react";
import axios from "axios";
import { useAuthHeader } from "react-auth-kit";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import CircularProgress from "@mui/material/CircularProgress";
import Box from "@mui/material/Box";
import CloseIcon from "@mui/icons-material/Close";
import PublicIcon from "@mui/icons-material/Public";
import LockIcon from "@mui/icons-material/Lock";
import FileUploadIcon from "@mui/icons-material/FileUpload";




const UploadFile = (props) => {
  const API_URL = import.meta.env.VITE_API_URL ?? 'http://localhost:5000/';
  
  const [loading, setLoading] = useState(false);
  const [vpublic, setvPublic] = useState(true);
  const [file, setFile] = useState("");
  

  const authHeader = useAuthHeader();
  
  let config = {
    headers: {
      Authorization: authHeader(),
    },
  };

  const onFileChange = (e) => {
    setFile(e.target.files[0]);
  };

 

  const upload = async (e) => {
    let result = Boolean;
    if (file === "") {
      toast('❌ Please select a file to upload!!', {
        position: "bottom-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "dark",
        });
      return;
    }
    e.preventDefault();
    const formData = new FormData();
    formData.append("file", file);
    formData.append("public", vpublic);
    setLoading(true);
    await axios
      .post(`${API_URL}files`, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: authHeader()
        },
      })
      
        
          result(true);
          setLoading(false);
          toast("✔ Upload Completed Successfully!", {
            position: "bottom-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "dark",
            });
            props.onModalChange(false);
        
     
      if (result){
        props.refresh();
      }
  };

  return (
    <>
    
        <ToastContainer />
      

      {loading ? <div></div> : <div></div>}
      {props.showModal ? (
        <>
          <div className="fixed inset-0 z-10 overflow-y-auto ">
            <div
              className="fixed inset-0 w-full h-full bg-black opacity-40 "
              onClick={() => () => props.onModalChange(false)}
            ></div>
            <div className="flex items-center min-h-screen px-4 py-8 ">
              <div className="relative w-full max-w-lg p-4 mx-auto bg-white rounded-xl shadow-lg ">
                <div className="mt-3 sm:flex justify-center">
                  {loading ? (
                    <div className="flex flex-col justify-center items-center">
                      <Box sx={{ display: "flex" }}>
                        <CircularProgress  className="items-center"/>
                      </Box>
                      <div className="flex">
                        <p>Your file is being uploaded, please wait....</p>
                      </div>
                    </div>
                  ) : (
                    <div className="mt-2 text-center sm:ml-4 sm:text-left">
                      <form onSubmit={upload}>
                        <input
                          className="form-control file:mr-5 file:py-2 file:px-6
                                    file:rounded-xl file:border-0
                                    file:text-sm 
                                    file:bg-purple-600 rounded-md file:text-white file:font-bold file:cursor-pointer file:hover:bg-purple-900"
                          name="picture"
                          type="file"
                          onChange={onFileChange}
                          id="formFile"
                        />

                        <h4 className="text-lg mt-6 font-medium text-gray-800">
                          Select File Access
                        </h4>
                        <div className="flex items-center mb-2"></div>
                        <div className="flex items-center ">
                          <input
                            id="default-radio-2"
                            defaultChecked
                            type="radio"
                            value=""
                            name="default-radio"
                            onClick={() => setvPublic(true)}
                          />
                          <label
                            htmlFor="default-radio-2"
                            className="ml-2 text-sm font-medium "
                          >
                            <PublicIcon className="text-blue-500" /> Public
                          </label>

                          <input
                            id="default-radio-1"
                            type="radio"
                            value=""
                            name="default-radio"
                            className="ml-6"
                            onClick={() => setvPublic(false)}
                          />
                          <label
                            htmlFor="default-radio-1"
                            className="ml-2 text-sm font-medium  "
                          >
                            <LockIcon className="text-red-500" /> Private
                          </label>
                        </div>
                      </form>
                      <p className="mt-2 text-[15px] leading-relaxed text-gray-500">
                        <strong>Public</strong> access will allow all registered
                        users to download the file, meanwhile{" "}
                        <strong>Private</strong> access will only aloud users
                        with the download link to be able to download the file.
                      </p>
                      <div className="items-center gap-2 mt-3 sm:flex">
                        <button
                          type="submit"
                          className="w-full mt-2 p-2.5 flex-1  bg-green-600 hover:bg-green-900 text-white font-bold rounded-md outline-none border ring-offset-2 ring-indigo-600 focus:ring-2 "
                          onClick={upload}
                        >
                          <FileUploadIcon /> Upload
                        </button>
                        <button
                          className="w-full mt-2 p-2.5 flex-1 text-white bg-red-600 rounded-md outline-none ring-offset-2 ring-red-600 focus:ring-2
                        hover:bg-red-900
                        "
                          onClick={() => props.onModalChange(false)}
                        >
                          <CloseIcon /> Cancel
                        </button>
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </>
      ) : null}
    </>
  );
};

export default UploadFile;
