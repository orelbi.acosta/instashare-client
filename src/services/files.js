import axios from "axios"

const API_URL = import.meta.env.VITE_API_URL ?? 'http://localhost:5000/';
const config = {
  headers: { Authorization: `Bearer ${localStorage.getItem("_auth")}` },
};

export const getPublicFiles = async () =>{
  const configx = {
    headers: { Authorization: `Bearer ${localStorage.getItem("_auth")}` },
  };
  const {data} = await axios.get(`${API_URL}files`,configx)
  return data

}

export const getMyFiles = async () =>{
  const configx = {
    headers: { Authorization: `Bearer ${localStorage.getItem("_auth")}` },
  };
  const {data} = await axios.get(`${API_URL}files/myfiles`, configx)
  return data
}
export const updateFile = async (id, isPublic) =>{
  const {data} = await axios.patch(`${API_URL}files/${id}`, {public:isPublic} ,config)
  return data
}

export const downloadFile = async (fileid, filename) => {
  //const response = await axios.get(`${API_URL}files/${fileid}`, config)
  axios({
    url: `${API_URL}files/${fileid}`,
    method: "GET",
    responseType: "blob",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("_auth")}`,
    },
  }).then((response) => {
    const href = URL.createObjectURL(response.data);
    const link = document.createElement("a");
    link.href = href;
    link.setAttribute("download", `${filename}`);
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    URL.revokeObjectURL(href);
  });
};

export const deleteFile = async (fileid) => {
  await axios.delete(`${API_URL}files/${fileid}`, config);
  return
};