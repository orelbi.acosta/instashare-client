import React, { useEffect, useState } from 'react'
import UploadFile from "../components/UploadFile";
import {getMyFiles,updateFile, downloadFile, deleteFile} from '../services/files'
import {formatDate} from '../utils/misc'

import FileUploadIcon from "@mui/icons-material/FileUpload";
import FolderZipIcon from "@mui/icons-material/FolderZip";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import DeleteIcon from "@mui/icons-material/Delete";

// Confirm Dialog
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';

const Myfiles = () => {
  const [myFiles, setMyFiles] = useState([{}]);
  const [showModal, setShowModal] = useState(false);
  const [open, setOpen] = React.useState(false);
  const [fileid, setFileid]=useState('');

  const handleClickOpen = (fileid) => {
    setOpen(true);
    setFileid(fileid);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
  });
  function onModalChange(value) {
    setShowModal(value);
  }

  const deleteF = async (fileid)=>{
    await deleteFile(fileid);
    loadMyFiles();
  }
  const loadMyFiles = async () =>{
    const result = await getMyFiles();
    setMyFiles(result);
    
  }
  const setPublic = async (e, id) => {
    if (e.target.checked) {
        await updateFile(id,true)
        loadMyFiles()
    }else{
      await updateFile(id,false)
      loadMyFiles()
    }
  }

  useEffect(() => {
    loadMyFiles();
  }, [setMyFiles])

  return (
    <div>
       <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          <div className='flex justify-center'>
          {"Delete File"}

          </div>
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
          Are you absolutely sure you want to delete the selected file?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <button onClick={handleClose} className='bg-purple-600 p-2 rounded text-white font-bold hover:bg-red-600'>
          ❌ Cancel
            </button>
          <button className='bg-purple-600 p-2 rounded text-white font-bold hover:bg-green-600'
          onClick={()=>{
            deleteF(fileid);
            handleClose();
          }} autoFocus>
            
             ✔️ Accept
           
          </button>
        </DialogActions>
      </Dialog>
       <UploadFile
          showModal={showModal}
          onModalChange={onModalChange}
          refresh={loadMyFiles}
        />
      <div className="flex justify-center mt-2">
        
      </div>
      <div className="max-w-4xl mx-auto mt-2">
        <div className="flex flex-col ">
          <div className="flex justify-between items-center">
          <h1 className="font-bold text-xl text-black-600 ml-2 text-blue-500">My Files</h1>
            <button
              className="p-2 mb-2 text-purple-100 bg-purple-600 rounded-md  hover:bg-purple-900"
              type="button"
              onClick={() => {
                setShowModal(true);
              }}
            >
              <FileUploadIcon /> Upload File
            </button>
          </div>

          <div className="overflow-x-auto shadow-md sm:rounded-lg">
            <div className="inline-block min-w-full align-middle">
              <div className="overflow-hidden ">
                <table className="min-w-full divide-y divide-gray-200 table-fixed dark:divide-gray-700">
                  <thead className="bg-gray-100 dark:bg-gray-700">
                    <tr>
                      <th scope="col" className="p-4">
                        <div className="flex items-center"></div>
                      </th>
                      <th
                        scope="col"
                        className="py-3 text-center px-6 text-xs font-medium tracking-wider text-gray-700 uppercase dark:text-gray-400"
                      >
                        File
                      </th>
                      <th
                        scope="col"
                        className="py-3 px-6 text-center text-xs font-medium tracking-wider text-gray-700 uppercase dark:text-gray-400"
                      >
                        Uploaded at
                      </th>
                      <th
                        scope="col"
                        className="py-3 px-6 text-xs font-medium tracking-wider text-center text-gray-700 uppercase dark:text-gray-400"
                      >
                        Size
                      </th>
                      <th
                        scope="col"
                        className="py-3 px-6 text-xs font-medium tracking-wider text-center text-gray-700 uppercase dark:text-gray-400"
                      >
                        Public
                      </th>
                      <th
                        scope="col"
                        className="py-3 px-6 text-xs font-medium tracking-wider text-center text-gray-700 uppercase dark:text-gray-400"
                      >
                        Actions
                      </th>
                    </tr>
                  </thead>
                  {myFiles.map((file, index) => (
                    <tbody
                      className="bg-white divide-y divide-gray-200 dark:bg-gray-800 dark:divide-gray-700"
                      key={index}
                    >
                      <tr className="hover:bg-gray-100 dark:hover:bg-gray-700 cursor-pointer hover:text-orange-500">
                        <td className="p-4 w-4 ">
                          <div className="flex items-center">
                            <button
                              className="text-white p-1 font-bold text-[11px] uppercase rounded-xl "
                              onClick={() => downloadFile(file.fileId, file.filename)}
                            >
                              <FileDownloadIcon className="hover:text-orange-500" />
                            </button>
                          </div>
                        </td>
                        <td className="py-4 px-6 text-sm font-medium text-gray-900 whitespace-nowrap dark:text-white cursor-pointer hover:text-orange-500">
                          <FolderZipIcon /> {file.filename}
                        </td>
                        <td className="py-4 px-6 text-sm text-center font-medium text-gray-500 whitespace-nowrap dark:text-white">
                          <p>{formatDate(file.createdAt)}</p>
                        </td>
                        <td className="py-4 px-6 text-center text-sm font-medium text-gray-500 whitespace-nowrap dark:text-white">
                          <p>{file.size} </p>
                        </td>
                        <td className="py-4 px-6 text-sm font-medium text-gray-500 whitespace-nowrap dark:text-white text-center">
                          <p>
                            <input
                              type="checkbox"
                              checked={file.public | false}
                              onChange={(e) => setPublic(e, file._id)}
                            />
                          </p>
                        </td>
                        <td className="py-4 px-6 text-center text-sm font-medium text-gray-500 whitespace-nowrap dark:text-white">
                          <p>
                            <DeleteIcon
                              className="hover:text-red-600 cursor-pointer"
                              onClick={() => {
                                handleClickOpen(file.fileId)
                                //deleteF(file.fileId);
                                
                              }}
                            />
                          </p>
                        </td>
                      </tr>
                    </tbody>
                  ))}
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  )
}

export default Myfiles