import React, { useEffect, useState } from "react";
import UploadFile from "../components/UploadFile";
import {getPublicFiles, downloadFile} from "../services/files";
import {formatDate} from '../utils/misc'


// Icons
import FolderZipIcon from "@mui/icons-material/FolderZip";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import FileUploadIcon from "@mui/icons-material/FileUpload";
import AutorenewIcon from '@mui/icons-material/Autorenew';

const Home = () => {
  const [files, setFiles] = useState([{}]);
  const [showModal, setShowModal] = useState(false);
  
  function onModalChange(value) {
    setShowModal(value);
  }
  const loadPublicFiles = async () =>{
    const result = await getPublicFiles();
    setFiles(result);
  }

  
  useEffect(() => {
    loadPublicFiles();
  }, [setFiles])
  

  return (
    <div>
      <UploadFile
          showModal={showModal}
          onModalChange={onModalChange}
          refresh={loadPublicFiles}
        />
      <div className="max-w-4xl mx-auto mt-4">
        <div className="flex flex-col ">
        <div className="flex justify-end ">
        <button
              className="p-2 mb-2 text-purple-100 bg-purple-600 rounded-md  hover:bg-orange-600"
              type="button"
              onClick={() => {
                setShowModal(true);
              }}
            >
              <FileUploadIcon /> Upload File
            </button>
        </div>
       <div className="overflow-x-auto shadow-md sm:rounded-lg">
            <div className="inline-block min-w-full align-middle">
              <div className="overflow-hidden ">
                <table className="min-w-full divide-y divide-gray-200 table-fixed dark:divide-gray-700">
                  <thead className="bg-gray-100 dark:bg-gray-700">
                    <tr>
                      <th scope="col" className="p-4">
                        <div className="flex items-center"></div>
                      </th>
                      <th
                        scope="col"
                        className="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400"
                      >
                        File
                      </th>
                      <th
                        scope="col"
                        className="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400"
                      >
                        Uploaded by
                      </th>
                      <th
                        scope="col"
                        className="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400"
                      >
                        Uploaded at
                      </th>
                      <th
                        scope="col"
                        className="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400"
                      >
                        Size
                      </th>
                    </tr>
                  </thead>
                  {files.map((file, index) => (
                    <tbody
                      className="bg-white divide-y divide-gray-200 dark:bg-gray-800 dark:divide-gray-700"
                      key={index}
                    >
                      <tr className="hover:bg-gray-100 dark:hover:bg-gray-700 cursor-pointer hover:text-orange-500">
                        <td className="p-4 w-4 ">
                          <div className="flex items-center">
                            <button
                              className="text-white p-1 font-bold text-[11px] uppercase rounded-xl "
                              onClick={() => downloadFile(file.fileId, file.filename)}
                            >
                              <FileDownloadIcon className="hover:text-orange-500" />
                            </button>
                          </div>
                        </td>
                        <td className="py-4 px-6 text-sm font-medium text-gray-900 whitespace-nowrap dark:text-white cursor-pointer hover:text-orange-500">
                          <FolderZipIcon /> {file.filename}
                        </td>
                        <td className="py-4 px-6 text-sm font-medium text-gray-500 whitespace-nowrap dark:text-white">
                          <p>{file.username}</p>
                        </td>
                        <td className="py-4 px-6 text-sm font-medium text-gray-500 whitespace-nowrap dark:text-white">
                          <p>{formatDate(new Date(file.createdAt))} </p>
                        </td>
                        <td className="py-4 px-6 text-sm font-medium text-gray-500 whitespace-nowrap dark:text-white">
                          <p>{file.size} </p>
                        </td>
                      </tr>
                    </tbody>
                  ))}
                </table>
              </div>
            </div>
          </div>
        </div>
        </div>
        </div>
      
  );
};

export default Home;
